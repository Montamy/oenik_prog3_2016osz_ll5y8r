﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace masolat
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public static BusinessLogic BL;
        public static int jatek;
        public static Random rnd = new Random();
        public static Canvas ter;
        
       
        public MainWindow()
        {
            jatek = 0;
            
            InitializeComponent();
            BL = new BusinessLogic((int)ActualWidth);
            this.DataContext = BL.VM;
            this.Background = BL.VM.BackGround;
            ter = vaszon;           
            MouseDown += BL.VillamBecsap;
           
        }

       



   
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            
            
            BL.Start();
            this.Background = BL.VM.BackGround;
            BL.ActualHossz = (int)ActualWidth;
            
        }
    }
}
