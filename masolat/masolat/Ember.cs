﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace masolat
{
    public class Ember : Bindable
    {
        static Random rnd = new Random();
        int dx, dy;
        public const int R = 40;
        public Ember(int hossz)
        {
            dx = 0; dy = 1;         //mozgás iránya, sebessége
            Init(hossz);
        }

        Rect alak;
        public Rect Alak { get { return alak; } set { alak = value; OPC(); } }
        public Brush Szin { get; set; }

        public ImageBrush KepEmber { get; set; }

        

        public void Init(int hossz)
        {
            Alak = new Rect(rnd.Next(0, hossz), 0, R, R);
            
            Szin = new SolidColorBrush(Color.FromRgb((byte)rnd.Next(0, 256), (byte)rnd.Next(0, 256), (byte)rnd.Next(0, 256)));
            KepEmber = new ImageBrush(new BitmapImage(new Uri("blue_man_01.png", UriKind.Relative)));
        }

        public int Mozog(int cw, int ch, Villam u)
        {

            Alak = new Rect(Alak.X + dx, Alak.Y + dy, Alak.Width, Alak.Height);           
            if (Alak.Top <= 0 || Alak.IntersectsWith(u.Alak))
            {
                return 2;
            }

            if (Alak.Left <= 0 || Alak.Right >= cw)
                dx = -dx;
            if (Alak.Top >= ch)      
                return 1;
            return 0;
        }
    }
}
