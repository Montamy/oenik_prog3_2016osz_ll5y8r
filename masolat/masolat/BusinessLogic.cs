﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace masolat
{
    
    public class BusinessLogic : Bindable
    {
        public static Random rnd = new Random();

        Stopwatch sw;
        DispatcherTimer dt;

        public event EventHandler JatekVege;
        public BusinessLogic(int hossz)
        {
            VM = new ViewModel();
            VM.Palya = 0;
            VM.Start = Visibility.Visible;
            VM.Emberek = new ObservableCollection<Ellipse>();
            VM.Villam = new Villam();
            VM.VillamVis = Visibility.Collapsed;
            VM.VillKinezet = new ImageBrush(new BitmapImage(new Uri("lightning_01.png", UriKind.Relative)));
            hanyadikvillam = 1;
            ActualHossz = hossz;
            JatekVege += BusinessLogic_JatekVege;
        }

        void BusinessLogic_JatekVege(object sender, EventArgs e)
        {
            sw.Stop();
            dt.Stop();
            MessageBox.Show("Game Over!\nFelhasznált idő: " + sw.ElapsedMilliseconds / 1000.0 + " másodperc");
            
        }
        public ViewModel VM { get; set; }
        public int ActualHossz { get; set; }

        public static Brush GetImage(string path, int mi)
        {
            int x = 0;
            int y = 0;
            if (mi == 0)
            {
                x = 960;
                y = 720;
            }
            if (mi == 1)
            {
                x = 200;
                y = 80;
            }


            ImageBrush ib = new ImageBrush(new BitmapImage(new Uri(path, UriKind.Relative)));
            ib.Viewport = new Rect(0, 0, x, y);
            ib.ViewportUnits = BrushMappingMode.Absolute;
            return ib;
        }

        public static Brush GetBack(int palya)
        {
            if (palya == 0)
                return GetImage("Menu.png", 0);
            else
                return GetImage("hatter_" + palya + ".png", 0);
        }

        private int hanyadikvillam;

        


        /* public void VillamValtas(object sender, EventArgs e)
         {



             VM.VillKinezet = new ImageBrush(new BitmapImage(new Uri("lightning_0" + hanyadikvillam + ".png", UriKind.Relative)));
             hanyadikvillam++;
             if (hanyadikvillam == 5)
                 hanyadikvillam = 1;
            
         }

         public void EmberMozgas()
         {
             VM.VillKinezet = new ImageBrush(new BitmapImage(new Uri("lightning_0" + hanyadikvillam + ".png", UriKind.Relative)));
             hanyadikvillam++;
             if (hanyadikvillam == 5)
                 hanyadikvillam = 1;
             OPC();
         }
     */

        public void VillamBecsap(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (MainWindow.jatek == 1)
            {
                VM.Villam.Mozog((int)e.GetPosition(MainWindow.ter).X, (int)e.GetPosition(MainWindow.ter).Y);
                VM.VillamVis = Visibility.Visible;
                VM.Aktiv = 5;
            }
        }

        public void Start()
        {
            Random rnd = new Random();
            VM.Palya = rnd.Next(1, 4);           
            VM.Start = Visibility.Hidden;

            MainWindow.jatek = 1;

            dt = new DispatcherTimer();
            dt.Interval = TimeSpan.FromMilliseconds(20);
            dt.Tick += dt_Tick;

            //   dt.Tick += BL.VillamValtas;
            dt.Start();
            EmberAdd();
            sw = new Stopwatch();
            sw.Start();
            
        }

        private void EmberAdd()
        {
            Ellipse e = new Ellipse();
            Ember g = new Ember(ActualHossz);
            e.DataContext = g;  //igy direktben hivatkozhatok a g propertyjeire
            e.SetBinding(Canvas.LeftProperty, new Binding("Alak.X"));
            e.SetBinding(Canvas.TopProperty, new Binding("Alak.Y"));
            e.SetBinding(Ellipse.WidthProperty, new Binding("Alak.Width"));
            e.SetBinding(Ellipse.HeightProperty, new Binding("Alak.Height"));
            e.SetBinding(Ellipse.FillProperty, new Binding("KepEmber"));
            VM.Emberek.Add(e);
            MainWindow.ter.Children.Add(e);
        }

        private void dt_Tick(object sender, EventArgs e)
        {
            for (int i = 0; i < VM.Emberek.Count; i++)
            {

                int help = (VM.Emberek[i].DataContext as Ember).Mozog((int)MainWindow.ter.ActualWidth, (int)MainWindow.ter.ActualHeight, VM.Villam);
                if (help == 1) //kiér a pályáról
                {
                    VM.Pontok--;
                    MainWindow.ter.Children.Remove(VM.Emberek[i]);
                    VM.Emberek.Remove(VM.Emberek[i]);
                }
                if (help == 2 && VM.VillamVis == Visibility.Visible) //ki kell törölni a listából
                {

                    
                    MainWindow.ter.Children.Remove(VM.Emberek[i]);
                    VM.Emberek.Remove(VM.Emberek[i]);
                    VM.Pontok++;
                }
                if(help == 0)
                {
                    JelmeztValt(VM.Emberek[i]);
                }
            }

            if (rnd.Next(0, 20) == 2)
            {
                EmberAdd();
            }
            if (VM.Aktiv != 0)
            {
                VM.Aktiv--;
                if (VM.Aktiv == 0)
                    VM.VillamVis = Visibility.Hidden;
            }
            if (VM.Pontok >= 30)
                JatekVege(this, EventArgs.Empty);
        }

        private void JelmeztValt(Ellipse a)
        {


        }


        
    }

   
}
