﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace masolat
{
    public class Villam : Bindable
    {
        public const int a = 100;

        public Villam()
        {
            Alak = new Rect(300, 300, a, a);
        }

        Rect alak;
        public Rect Alak { get { return alak; } set { alak = value; OPC(); } }

        public void Mozog(int dx, int dy)
        {

            Alak = new Rect(dx - 15 , dy - a + 15 , Alak.Width, Alak.Height);
        }
    }
}
