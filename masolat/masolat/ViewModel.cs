﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows.Shapes;

namespace masolat
{
    public class ViewModel : Bindable
    {
        //egyke
        private static ViewModel _peldany;
        public static ViewModel Get()
        {
            if (_peldany == null)
            {
                _peldany = new ViewModel();

            }

            return _peldany;
        }
        //konstruktor
        public ViewModel()
        {

        }
        //adatok
        int pontok;
        public int Pontok { get { return pontok; } set { pontok = value; OPC(); } }

        //emberek a pályán
        public ObservableCollection<Ellipse> Emberek { get; set; }

        //villám
        public Villam Villam { get; set; }

        Visibility villamvis;
        public Visibility VillamVis { get { return villamvis; } set { villamvis = value; OPC(); } }

        int aktiv;

        public int Aktiv { get { return aktiv; } set { aktiv = value; } }

        ImageBrush villKinezet;

        public ImageBrush VillKinezet { get { return villKinezet; } set { villKinezet = value; } }


        //ez kell ?
        /*  int villam_x, villam_y;
          public int Villam_X { get { return villam_x; } set { villam_x = value; OPC(); } }
          public int Villam_Y { get { return villam_y; } set { villam_y = value; OPC(); } } */

        //hanyadik pálya
        int palya;
        public int Palya { get { return palya; } set { palya = value; OPC(); } }

        //start gomb visbility 
        Visibility start;
        public Visibility Start { get { return start; } set { start = value; OPC(); } }

        public Brush BackGround { get { return BusinessLogic.GetBack(Palya); } }
        public Brush Buttons { get { return BusinessLogic.GetImage("time_on.png", 1); } }

       
    }
}
