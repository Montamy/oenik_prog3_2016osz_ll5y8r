﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;

namespace beadando
{
    public class ViewModel
    {
        private static ViewModel _peldany;

        public static ViewModel Get()
        {
            if(_peldany == null)
            {
                _peldany = new ViewModel();                               
                
            }
            
            return _peldany;
        }


        public int Palya {get; set;}

        public Brush BackGround { get { return BusinessLogic.GetBack(Palya); } }
        public Brush Buttons { get { return BusinessLogic.GetImage("time_on.png", 1); } }
    }
}
