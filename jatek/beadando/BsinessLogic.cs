﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace beadando
{
    public class BusinessLogic
    {
        public BusinessLogic()
        {
            VM = new ViewModel();
        }


        public ViewModel VM { get; set; }

        public static Brush GetImage(string path,int mi )
        {
            int x = 0;
            int y = 0;
            if(mi==0)
            {
                x = 960;
                y = 720;
            }
            if (mi == 1)
            {
                x = 200;
                y = 80;
            }
               

            ImageBrush ib = new ImageBrush(new BitmapImage(new Uri(path, UriKind.Relative)));
            ib.Viewport = new Rect(0, 0, x, y);
            ib.ViewportUnits = BrushMappingMode.Absolute;
            return ib;
        }

        public static Brush GetBack(int palya)
        {
            if (palya == 0)
                return GetImage("Menu.png",0);
            else
                return GetImage( "hatter_"+ palya +".png",0);
        }
    }
}
